import "./Navigation.css";

function Navigation(props) {
  
  //on ajoute les props pour récupérer les données du parent par props drilling
  const { user, setUser } = props;

  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      
      {/* si user est défini alors retourne la <li> contenant le nom, sinon celle demandant de se connecter*/}
      {user ? <li>{user?.name}</li> : <li>Please log in</li>}
    </ul>
  );
}

export default Navigation;
