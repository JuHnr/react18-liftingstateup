import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  //ce state permet de stocker les données récupérées de Profile par state lifting 
  //ces données sont ensuite transmises à tous les enfants de App par props drilling (et donc à Navigation)
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      {/*on ajoute les props à Navigation pour lui transmettre les données reçues par App*/}
      <Navigation user={user} setUser={setUser} />
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
